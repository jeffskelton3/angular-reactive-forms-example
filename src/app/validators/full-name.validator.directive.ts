import {Directive} from '@angular/core';
import {FormControl, NG_VALIDATORS, Validator} from '@angular/forms';

@Directive({
  selector: '[fullName]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: FullNameValidatorDirective,
    multi: true
  }]
})
export class FullNameValidatorDirective implements Validator {
  validate(c: FormControl): { [key: string]: any } {
    const wordCount = ((c.value || '').match(/\S+/g) || []).length;
    return wordCount > 1 ? null : {
      fullName: 'Please use your full name'
    };
  }
}
