import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WithFormControlExampleComponent } from './with-form-control-example/with-form-control-example.component';
import { CustomComponentComponent } from './with-form-control-example/custom-component/custom-component.component';
import {FullNameValidatorDirective} from './validators/full-name.validator.directive';


@NgModule({
  declarations: [
    AppComponent,
    WithFormControlExampleComponent,
    CustomComponentComponent,
    FullNameValidatorDirective
  ],
  imports: [
    ReactiveFormsModule, // <== we need to import this for our form builders
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
