import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithFormControlExampleComponent } from './with-form-control-example.component';

describe('WithFormControlExampleComponent', () => {
  let component: WithFormControlExampleComponent;
  let fixture: ComponentFixture<WithFormControlExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithFormControlExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithFormControlExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
