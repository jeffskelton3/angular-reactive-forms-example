import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators, NgForm, FormControl} from '@angular/forms';

@Component({
  selector: 'app-with-form-control-example',
  templateUrl: './with-form-control-example.component.html',
  styleUrls: ['./with-form-control-example.component.css']
})
export class WithFormControlExampleComponent implements OnInit {

  // by setting #myForm="ngForm" in our template, we can access higher level form state.
  // for our purposes we want to know whether or not the form was submitted
  @ViewChild('myForm') myForm: NgForm;

  formGroup: FormGroup;

  constructor(protected formBuilder: FormBuilder) {
  }

  ngOnInit() {

    // set default value for controls
    // set default validation state for controls
    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      isRyanEmployee: [false]
    });

    // we can watch for value changes on any control we want and update our forms rules
    // accordingly. In this case I want to change some validation rules based upon
    // whether or not this user is a ryan employee.
    this.formGroup.get('isRyanEmployee').valueChanges.subscribe((val: boolean) => {
      const defaultValidators = [Validators.required, Validators.email];
      const validators = val ? defaultValidators.concat(this.isRyanEmailValidator) : defaultValidators;
      this.formGroup.get('email').setValidators(validators);
      this.formGroup.get('email').updateValueAndValidity();

      // we can even dynamically add/remove controls from our form.
      if (val) {
        this.formGroup.addControl('ryanProduct', new FormControl('', [Validators.required]));
      } else {
        this.removeOptionalControls();
      }
    });
  }

  removeOptionalControls () {
    const ctrl = this.formGroup.get('ryanProduct');
    if (ctrl) {
      this.formGroup.removeControl('ryanProduct');
    }
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.formGroup.valid) {

      // here we gather up all the values in the form and put them into an array.
      // normally this would be where you would format them for an ajax call etc
      const values = Object.keys(this.formGroup.controls)
        .map(k => `${k}: ${this.formGroup.get(k).value}`);

      // just display the form values we submitted.
      alert(`You submitted: ${JSON.stringify(values, null, 2)}`);

      this.removeOptionalControls();
      this.myForm.resetForm(); // <== clears out all the form values and resets validation state. Neat!
    } else {
      this.formGroup.markAsDirty(); // <== we can manually mark a form as dirty
    }
  }

  // angular automatically adds css classes to our controls depending on their state, but we might want to add some additional
  // markup elsewhere. We can leverage something like this in our template to conditionally add/remove css or control specific
  // error messaging
  public showControlError(controlName: string): boolean {
    const control = this.formGroup.get(controlName);
    return !control.valid && this.myForm.submitted;
  }

  // our validators provide us with a list of errors that we can access throughout the
  // lifecycle of our form. When and how we display them is up to us.
  // here I am just gathering them up and creating an array of error messages.
  public get errorDisplay(): string[] {
    return Object.keys(this.formGroup.controls).reduce((acc, c) => {
      const control = this.formGroup.controls[c];

      const errors = control.errors || {};

      if (errors.required) {
        acc.push(`${c} is a required field`);
      }

      if (errors.email && control.value) {
        acc.push(`"${control.value}" is not a valid email address`);
      }

      return acc.concat(Object.keys(errors)
        .filter(e => e !== 'required' && e !== 'email') // filter out the error messages we already dealt with
        .map(k => errors[k]));
    }, []);
  }

  public get displayErrorMessages(): boolean {
    return !this.formGroup.valid && this.myForm.submitted;
  }

  public get displayRyanProductDropdown(): boolean {
    return this.formGroup.get('isRyanEmployee').value;
  }

  // sometimes you need to implement custom validation rules.
  // here we are checking if the control contains '@ryan.com'
  // if the control passes our validation rules, we return null
  // if it does not, we return an error message. We control the format
  // of this error message. I usually like to return an object with
  // a string value containing the error I want to display.
  // Its completely up to the implementor though. Just be sure to
  // set your method signature accordingly.
  private isRyanEmailValidator(c: FormControl): { [key: string]: any } {
    const value = c.value ? c.value.trim().toLowerCase() : '';

    // you'd probably want to use regex or something here but you get the idea.
    const emailDomain = value.split('@').length === 2 ? value.split('@')[1] : '';
    return emailDomain === 'ryan.com' ? null : {
      isRyanEmployee: 'please use a valid ryan.com email address'
    };
  }

}
