import {Component, forwardRef, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const noop = () => {
};

@Component({
  selector: 'app-custom-component',
  templateUrl: './custom-component.component.html',
  styleUrls: ['./custom-component.component.css'],
  // HERE IS WHERE THE MAGIC HAPPENS.
  // WE HOOK INTO THE VALUE ACCESSOR TOKEN
  // WHICH IN TURN ALLOWS US TO ATTACH THIS COMPONENT
  // INTO A REACTIVE FORM
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomComponentComponent),
    multi: true
  }]
})
export class CustomComponentComponent implements ControlValueAccessor {
  // Placeholders for the callbacks which are later provided
  // by the Control Value Accessor
  public onTouchedCallback: () => void = noop;
  public onChangeCallback: (_: any) => void = noop;

  protected _value = '';
  protected _disabled = false;

  constructor() {
  }

  public onChange(e): void {
    this.onChangeCallback(e.target.value);
  }

  // control value accessor methods
  protected set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChangeCallback(v);
    }
  }

  protected get value() {
    return this._value;
  }

  protected onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(value: any): void {
    if (value !== this._value) {
      this._value = value;
    }
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  public setDisabledState(isDisabled) {
    this._disabled = isDisabled;
  }

  public get isDisabled(): boolean {
    return this._disabled;
  }

}
